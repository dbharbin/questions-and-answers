#!/bin/bash
#
# This script converts a text file listing projects to a manifest xml
# file, which can be used by Gitlab to import these projects as batch.
# The required format of the manifest xml can be found below.
#
#   https://docs.gitlab.com/ee/user/project/import/manifest.html#manifest-format
#
# Gitlab has a limitation on project name, that is project name cannot
# be same as folder/group name at same level.  That's called name clash
# between project and folder/group.  The following is a real example
# from AOSP.
#
#   platform/build
#   platform/build/blueprint
#   platform/build/kati
#   platform/build/soong
#
# The 'build' is firstly the name of a project (git repository), but
# in the meantime it's also the name of the folder/group holding
# projects 'blueprint', 'kati' and 'soong'.  In this case, when we
# import project 'build' into Gitlab, we will need to rename it to
# 'build_git', and thus we will have the following projects in Gitlab.
#
#   platform/build_git
#   platform/build/blueprint
#   platform/build/kati
#   platform/build/soong

# Text file listing all projects to be imported
LIST="download.txt"

# Manifest file generated for Gitlab use
MANIFEST="manifest.xml"

# Intermediate file listing projects with name clash issue
CLASH="clash.txt"

# Intermediate file listing subgroups
SUBGROUPS="subgroups.txt"

# URL of source repositories
URL="https://android.googlesource.com/"

# Suffix to rename the project
SUFFIX="_git"

if [ ! -f $LIST ]; then
  echo "File $LIST does not exist"
  exit 1
fi

if [ -f $MANIFEST ]; then
  echo "File $MANIFEST already exists"
  exit 1
fi

if [ -f $CLASH ]; then
  rm $CLASH
fi
touch $CLASH

if [ -f $SUBGROUPS ]; then
  rm $SUBGROUPS
fi
touch $SUBGROUPS

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" >> $MANIFEST
echo "<manifest>" >> $MANIFEST
echo "  <remote review=\"$URL\" />" >> $MANIFEST

while read LINE
do
  if grep -q "^$LINE/" $LIST; then
    echo -n "  <project name=\"$LINE\" path=\"$LINE" >> $MANIFEST
    echo "$SUFFIX\" />" >> $MANIFEST
    echo $LINE >> $CLASH
  else
    echo "  <project name=\"$LINE\" path=\"$LINE\" />" >> $MANIFEST
  fi

  PATHNAME="$(dirname $LINE)"
  while [ "$PATHNAME" != '.' ];
  do
    if ! grep -q "^$PATHNAME$" $SUBGROUPS; then
      echo $PATHNAME >> $SUBGROUPS
    fi
    PATHNAME="$(dirname $PATHNAME)"
  done
done < $LIST

echo "</manifest>" >> $MANIFEST
