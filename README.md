# GitLab Questions and Answers

This Readme provides answers to several key questons:


**Table of Contents**




## How to move a project from private to public
This id done using the "visibility" field

### UI Method
Beside the project name, hover over the icon. A description box will appear stating current repo visibility state. To change visibility, go to project settings-visibility and change it. Use the same method to change a project from public to private or private to public. **Don't forget to save!**
See example below:


<img src=photos/PrivateToPublic.gif width=40% height=50% />


### API Method

For example, the following curl command can change project
https://gitlab.com/aosp_test/platform/manifests to public by calling
Gitlab [Projects API](https://docs.gitlab.com/ee/api/projects.html#project-visibility-level).

```
curl -s -X PUT -H "Content-Type: application/json" -d '{"visibility":"public"}' https://gitlab.com/api/v4/projects/aosp_test%2Fplatform%2Fmanifests?private_token=YOUR_TOKEN
```

The target URL of curl command starts with `https://gitlab.com/api/v4/projects/`.
And then it's project path aosp_test/platform/manifests with slash
replaced by %2F, thus `aosp_test%2Fplatform%2Fmanifests`. The last is
`?private_token=YOUR_TOKEN`, where YOUR_TOKEN is your [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token).

## Best way to manage related but separate code bases
It's recommended to use the Groups and SubGroups features in GitLab.  This allows for several advantages incluing:

- Easy to develop in a private repo and when ready, simply flip the visibility to public.
- [Granularity of security](https://docs.gitlab.com/ee/user/permissions.html "Click to see Permissions feature overview") increases with ability to control access to a project down to the individual level.
- Each code base can have it's own readme and wiki for errata's and other code base specific information that's desired
- Each code base can have it's own issue tracking

<!--
For photos format:    ![alt text](photos/colorisolationapp.png "HSV Tuning App")
-->


## How to make a public branch in a private repo
It isn't possible to have a branch with a different visibility than it's project.  However, if a user wishes, he or she can change the [default branch](https://docs.gitlab.com/ee/user/project/repository/branches/#default-branch) from Master.  Then, when the project is made public, all merge requests and changes will be made to the branch that is now the default.

## How to make a project part of a group


## How to revoke users from System from his or her email address

## How to revoke all users from a repo (effectively removing access from all users)


## How to see the last time a user accessed / cloned a repo


## What metrics are available


## How to see what repos a user has visibility to

### Project creation and replication examples

## How to import a Github repo

GitLab instructions [here](https://docs.gitlab.com/ee/user/project/import/github.html)

* Per above instructions, start by creating new project (note must be new, can't import if project already exists in GitLab)
* Click on the **import project** tab and then click "GitHub"
* GitLab will ask for GitHub credentials.  Must enter Github password and user name to authenticate
* GitLab will list all projects that can be imported.  This will include projects from any Organization you belong to on GitHub
* Seclect *import* associated with the project to import
* At top of page, a like will be created and say "running" while the import is happening.  This taks a bit.
* 

? what about wiki?, issues?, branches, merges, etc?

## Set up a mirror


## Copy a repository from GitHub to GitLab from the command line

First use the GitLab UI to create an empty project. This example created one called lx_kernel

Now open a command shell and do the following to clone a GitHub repo.
This example will clone the linux kernel from here: https://github.com/torvalds/linux.git
```
mkdir temp
cd temp
git clone https://github.com/torvalds/linux.git
cd linux
```

Next, set up username and email to match the GitLab instance. Then change origin to point to new empy GitLab repo and push content to the new repo
Note in this example, the url for the new/empty GitLab project is https://git.poc.itmethods.com/dbharbin/lx_kernel.git
```
git config --global --list
git config --global user.email "don.harbin@linaro.org"
git config --global user.name "dbharbin"
git remote -v

git remote set-url origin https://git.poc.itmethods.com/dbharbin/lx_kernel.git
```

Verify things set up right with correct username, email and pointing to new empty repo:
```
git config --global --list
git remote -v
```

Now push the git repo to the GitLab repository
```
git push -u origin --all
```


## System migration, clone and build use case
This section provides the steps and instructions to import (migrate) the MSM AOSP project, clone to local linux machine and successfully build the AOSP project.


## System on Android Mirror into Gitlab, clone, and build use case
This section provides the steps and instructions to mirror the custom AOSP project, then clone and successfully build the entire project.


## Google AOSP migration and build Use Case
This section provides the steps and instructions to import AOSP from Google source repo into GitLab, clone the GitLab AOSP project to a local Linux host, and then build the AOSP project in it's entirely.

**Generate manifest file for Gitlab project importing**

To import AOSP type of repository that consists of hundreds of projects,
we need to use a manifest file to do it as batch. The required format of
this manifest file can be found [here](https://docs.gitlab.com/ee/user/project/import/manifest.html#manifest-format).

Script [list_to_manifest.sh](scripts/list_to_manifest.sh) was created to
help compose the manifest file from a text listing all projects to be
imported. For example, if we have download.txt file listing projects
and in the same folder as list_to_manifest.sh, executing the script like
below will generate files: manifest.xml and clash.txt.

**Note**: The download.txt doesn't need to be manually composed, as Google
provides it **[here](https://android.googlesource.com/?format=TEXT)** for
Android source repository.
Also it's recommended to clone this repo and then to perform all the steps in the `./scripts` subdirectory including placement of the interim files like download.txt, clash.txt, default.xml, and manifest.xml. 

Once download.txt has been downloaded(by clicking the above link) and moved to the ./scripts directory, run the following script to create the manifest.xml required for the next step.
```
./list_to_manifest.sh
```

The manifest.xml is just the manifest file that will be used by Gitlab
for importing projects from Google source repository. Text clash.txt is
an intermediate file listing projects affected by Gitlab name clash.
Gitlab has a limitation on project name, that is project name cannot
be same as folder/group name at same level. See comments at header of
list_to_manifest.sh for details of Gitlab name clash limitation, and how
we work around it.

The script can be customized with the following variables, so that it
can work per user's need.

```
# Text file listing all projects to be imported
LIST="download.txt"
# Manifest file generated for Gitlab use
MANIFEST="manifest.xml"
# Intermediate file listing projects with name clash issue
CLASH="clash.txt"
# URL of source repositories
URL="https://android.googlesource.com/"
# Suffix to rename the project
SUFFIX="_git"
```


**Migrate AOSP from Google**
* Create a new Group in GitLab
* Select "New Project" in GitLab 
  * Select "Import Project"
  * Select the GitLab Group to import the AOSP projects into
  * Select "Manifest File"
  * Navigate to the manifest.xml file created in the previous step and select
  * Click "List available repositories"



<img src=photos/AOSPMigrateFromManifest.gif width=40% height=50% />


  * For the last step, click "Import all repositories" and wait. AOSP is large and can take some time to import into GitLab

**Fix up Android manifest for renamed projects**

With above project importing done successfully, the AOSP repository on
Gitlab is almost ready for use, only except that project
platform/manifest needs some fixup.  This is because the projects with
name clash issue get renamed with given suffix, i.e. "_git" in our
example, and thus we need to fix up the Android manifest file, so that
"repo sync" can find the projects. Script [manifest_fixup.sh](scripts/manifest_fixup.sh)
is there to help.  Let's say we created Group aosp_test2 in above
step and have AOSP projects imported in there. The following steps are
used to fix up default.xml file in platform/manifest project.

```
# Clone manifest project from Gitlab
git clone git@gitlab.com:aosp_test2/platform/manifest.git
cd manifest
# Create a new branch to maintain fixup modifications
git checkout -b gitlab/master origin/master
# Copy manifest file to scripts folder
cp default.xml <scripts folder>
cd <scripts folder>
# Execute the script to fix up default.xml in place
./manifest_fixup.sh
# Copy changed default.xml back to manifest project folder
cp default.xml <manifest folder>
cd <manifest folder>
# Commit and push manifest changes
git add default.xml
git commit
git push origin gitlab/master
```

Now we have modified default.xml on branch gitlab/master, and aosp_test2
repository is ready for use.

Note: there are a few variables in script manifest_fixup.sh. CLASH and
SUFFIX need to match the ones in list_to_manifest.sh.

```
# Manifest file to be fixed up
MANIFEST="default.xml"
# Intermediate file listing projects with name clash issue
CLASH="clash.txt"
# Suffix projects renamed with
SUFFIX="_git"
```
 
**Build the migrated AOSP project**

With above steps, we have Google AOSP repositories migrated from
https://android.googlesource.com/ to https://gitlab.com/aosp_test2/.
The following commands can be used to download and build the AOSP on
Gitlab then.

```
repo init -u https://gitlab.com/aosp_test2/platform/manifest
repo sync
source build/envsetup.sh
lunch
make -j<num>
```

**Enable mirroring of Gitlab projects**

Up till now, we have imported AOSP repositories from Google source to
Gitlab, and been able to download code from Gitlab and build it.
However, if Google source has update on its AOSP repositories, Gitlab
is not able to synchronize the changes yet, unless we enable mirroring
for these projects on Gitlab.

To enable mirroring for a single project, we can follow Gitlab document
[Repository mirroring](https://docs.gitlab.com/ee/workflow/repository_mirroring.html)
to do it with Gitlab web interface.  But the approach is unpractical for
our case - mirroring for hundreds of projects. Script [gitlab_mirroring.sh](scripts/gitlab_mirroring.sh)
was created to enable mirroring with Gitlab API.

To use Gitlab API, two pieces of Gitlab user information are needed:
personal access token and user ID. Follow Gitlab document
[Creating a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
to get a person token. For user ID, log into Gitlab and then goto
https://gitlab.com/api/v4/users?username=YOUR_USERNAME, where
YOUR_USERNAME is your Gitlab username, you will find it out.

```
# Text file listing all projects to be imported
LIST="download.txt"
# Intermediate file listing projects with name clash issue
CLASH="clash.txt"
# Suffix projects renamed with
SUFFIX="_git"
# Gitlab personal access token
TOKEN="xxxxxxxxxxxxxxxxxxxx"
# Gitlab user id
USERID="xxxxxxx"
# URL of source repositories
SRCURL="https://android.googlesource.com/"
# Gitlab group name of repositories
GROUP="aosp_test2"
```
Customize above variables in the script as needed, and then you are
ready to execute it as below.

```
./gitlab_mirroring.sh
```

Note: variables LIST, CLASH and SUFFIX should be set same as the ones
set in list_to_manifest.sh.
