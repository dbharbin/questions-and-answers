#!/bin/bash
#
# This script fixes up upstream manifest file to have the correct
# Gitlab 'path' for those projects with name clash.

# Manifest file to be fixed up
MANIFEST="default.xml"

# Intermediate file listing projects with name clash issue
CLASH="clash.txt"

# Suffix projects renamed with
SUFFIX="_git"

if [ ! -f $MANIFEST ]; then
  echo "File $MANIFEST does not exist"
  exit 1
fi

if [ ! -f $CLASH ]; then
  echo "File $CLASH does not exist"
  exit 1
fi

while read LINE
do
  # echo $LINE
  OLD="name=\\\""
  NEW="$OLD$LINE$SUFFIX\\\""
  OLD="$OLD$LINE\\\""
  SED="s/$OLD/$NEW/g"
  sed -i 's,'$OLD','$NEW',g' $MANIFEST
done < $CLASH
