#!/bin/bash

# Text file listing all projects to be imported
LIST="download.txt"

# Intermediate file listing projects with name clash issue
CLASH="clash.txt"

# Suffix projects renamed with
SUFFIX="_git"

# Gitlab personal access token
# https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
TOKEN="xxxxxxxxxxxxxxxxxxxx"

# Gitlab user id
# Log into Gitlab and go to https://gitlab.com/api/v4/users?username=YOUR_USERNAME,
# where YOUR_USERNAME is your Gitlab username, and you will find your user ID.
USERID="xxxxxxx"

# Gitlab top group name of repositories
TOPGRP="aosp_test2"

# Intermediate file listing subgroups
SUBGROUPS="subgroups.txt"

# Gitlab project visibility field is being changed to
VISIBILITY="public"

if [ ! -f $LIST ]; then
  echo "File $LIST does not exist"
  exit 1
fi

if [ ! -f $CLASH ]; then
  echo "File $CLASH does not exist"
  exit 1
fi

if [ ! -f $SUBGROUPS ]; then
  echo "File $SUBGROUPS does not exist"
  exit 1
fi

function update_groups() {
  echo "=== Update visibility of groups ==="
  NUMOFLINES=$(wc -l < $TEMP)
  i=1
  while read LINE
  do
    echo -n $LINE
    printf " [%d/%d]\n" $i $NUMOFLINES
    SUBGRP="${LINE//\//%2F}"
    CMDURL="https://gitlab.com/api/v4/groups/$TOPGRP%2F$SUBGRP?private_token=$TOKEN"
    DATA="{\"visibility\":\"$VISIBILITY\"}"
    curl -X PUT -H "Content-Type: application/json" -d $DATA $CMDURL &> /dev/null
    i=$((i+1))
  done < $TEMP
}

function update_projects() {
  echo "=== Update visibility of projects ==="
  NUMOFLINES=$(wc -l < $LIST)
  i=1
  while read LINE
  do
    echo -n $LINE
    printf " [%d/%d]\n" $i $NUMOFLINES
    PROJ="${LINE//\//%2F}"
    grep -q "^$LINE$" $CLASH && PROJ="$PROJ$SUFFIX"
    CMDURL="https://gitlab.com/api/v4/projects/$TOPGRP%2F$PROJ?private_token=$TOKEN"
    DATA="{\"visibility\":\"$VISIBILITY\"}"
    curl -X PUT -H "Content-Type: application/json" -d $DATA $CMDURL &> /dev/null
    i=$((i+1))
  done < $LIST
}

# Temporary file
TEMP="temp.txt"

if [ -f $TEMP ]; then
  rm $TEMP
fi

if [ $VISIBILITY = "public" ]; then
  # Parent groups go before child ones
  sort $SUBGROUPS | tee $TEMP > /dev/null
  # Change groups to public first and then projects
  update_groups
  update_projects
fi

if [ $VISIBILITY = "private" ]; then
  # Child groups go before parent ones
  sort -r $SUBGROUPS | tee $TEMP > /dev/null
  # Change projects to private and then groups
  update_projects
  update_groups
fi

rm $TEMP
