#!/bin/bash
#
# This script reads projects already imported into Gitlab and calls
# Gitlab API to enable mirroring for these projects.  The following
# is an example of curl command.
#
# curl -X PUT -H "Content-Type: application/json" \
#   -d '{"import_url":https://android.googlesource.com/accessories/manifest","mirror":true,"mirror_user_id":xxxxxxx}' \
#   https://gitlab.com/api/v4/projects/aosp_test2%2Faccessories%2Fmanifest?private_token=xxxxxxxxxxxxxxxxxxxx

# Text file listing all projects to be imported
LIST="download.txt"

# Intermediate file listing projects with name clash issue
CLASH="clash.txt"

# Suffix projects renamed with
SUFFIX="_git"

# Gitlab personal access token
# https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
TOKEN="xxxxxxxxxxxxxxxxxxxx"

# Gitlab user id
# Log into Gitlab and go to https://gitlab.com/api/v4/users?username=YOUR_USERNAME,
# where YOUR_USERNAME is your Gitlab username, and you will find your user ID.
USERID="xxxxxxx"

# URL of source repositories
SRCURL="https://android.googlesource.com/"

# Gitlab group name of repositories
TOPGRP="aosp_test2"

if [ ! -f $LIST ]; then
  echo "File $LIST does not exist"
  exit 1
fi

if [ ! -f $CLASH ]; then
  echo "File $CLASH does not exist"
  exit 1
fi

NUMOFLINES=$(wc -l < $LIST)
i=1

while read LINE
do
  echo -n $LINE
  printf " [%d/%d]\n" $i $NUMOFLINES
  PROJ="${LINE//\//%2F}"
  grep -q "^$LINE$" $CLASH && PROJ="$PROJ$SUFFIX"
  CMDURL="https://gitlab.com/api/v4/projects/$TOPGRP%2F$PROJ?private_token=$TOKEN"
  DATA="{\"import_url\":\"$SRCURL$LINE\",\"mirror\":true,\"mirror_user_id\":$USERID}"
  curl -X PUT -H "Content-Type: application/json" -d $DATA $CMDURL &> /dev/null
  i=$((i+1))
done < $LIST
